class CreateRegistrations < ActiveRecord::Migration
  def change
    create_table :registrations do |t|
      t.integer "conference_id"
      t.integer "user_id"
      t.integer "will_attend"
      t.date    "arrival_date"
      t.date    "departure_date"
      t.timestamps "datetime"
    end
  end
end
