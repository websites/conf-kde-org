class AddLdapUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :ldap_uid, :string
  end
end
