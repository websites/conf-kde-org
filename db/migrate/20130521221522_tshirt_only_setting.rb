class TshirtOnlySetting < ActiveRecord::Migration
  def up
    add_column :conferences, :enable_tshirt, :int
  end

  def down
  end
end
