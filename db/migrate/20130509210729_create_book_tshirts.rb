class CreateBookTshirts < ActiveRecord::Migration
  def change
    create_table :book_tshirts do |t|
      t.integer "user_id"
      t.integer "conference_id"
      t.string "size"
      t.string "attendance"
      t.timestamps
    end
  end
end
