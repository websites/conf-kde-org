class AddTravelNotesToSponsorship < ActiveRecord::Migration
  def change
  	add_column :sponsorships, :travel_notes, :text
  end
end
