class AddSponsorshipToRegistration < ActiveRecord::Migration
  def change
    add_column :registrations, :enable_sponsor, :integer
  end
end
