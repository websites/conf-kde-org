class CreateRegistrationLoggings < ActiveRecord::Migration
  def change
    create_table :registration_loggings do |t|
      t.integer "user_id"
      t.integer "will_attend"
      t.integer "current_count"
      t.timestamps
    end
  end
end
