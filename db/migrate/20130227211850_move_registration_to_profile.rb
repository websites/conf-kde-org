class MoveRegistrationToProfile < ActiveRecord::Migration
  def up
  	add_column :people, :role_artist, :int
  	add_column :people, :role_community, :int
  	add_column :people, :role_developer, :int
  	add_column :people, :role_promo, :int
  	add_column :people, :role_translator, :int
  	add_column :people, :role_user, :int
  	add_column :people, :role_other, :text
  	add_column :people, :dietery_gluten_free, :int
  	add_column :people, :dietery_lactose_free, :int
  	add_column :people, :dietery_nut_free, :int
  	add_column :people, :dietery_vegetarian, :int
  	add_column :people, :dietery_vegan, :int
  	add_column :people, :other_dietery, :text
  	add_column :people, :allergy, :text
  	add_column :people, :interests, :text
  	add_column :people, :emergency_contact, :text
  	add_column :people, :other_info, :text
  end

  def down
  end
end
