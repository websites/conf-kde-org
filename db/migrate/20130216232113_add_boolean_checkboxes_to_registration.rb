class AddBooleanCheckboxesToRegistration < ActiveRecord::Migration
  def change
    add_column :registrations, :role_artist, :integer
    add_column :registrations, :role_community, :integer
    add_column :registrations, :role_developer, :integer
    add_column :registrations, :role_promo, :integer
    add_column :registrations, :role_translator, :integer
    add_column :registrations, :role_user, :integer
    add_column :registrations, :dietery_gluten_free, :integer
    add_column :registrations, :dietery_lactose_free, :integer
    add_column :registrations, :dietery_nut_free, :integer
    add_column :registrations, :dietery_vegan, :integer
    add_column :registrations, :dietery_vegetarian, :integer
  end
end