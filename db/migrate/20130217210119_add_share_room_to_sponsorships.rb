class AddShareRoomToSponsorships < ActiveRecord::Migration
  def change
    add_column :sponsorships, :share_room, :string
  end
end
