class AddBioToEvents < ActiveRecord::Migration
  def change
    add_column :events, :bio, :string
  end
end
