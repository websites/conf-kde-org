class AddOtherPeopleToEvents < ActiveRecord::Migration
  def change
    add_column :events, :other_speakers, :string
  end
end
