class AddMoreFieldsToSponsorships < ActiveRecord::Migration
  def change
    add_column :sponsorships, :home_town, :text
    add_column :sponsorships, :mode_of_transport, :text
    add_column :sponsorships, :cost_percentage, :text
    add_column :sponsorships, :from_date, :date
    add_column :sponsorships, :to_date, :date
    add_column :sponsorships, :email, :text
  end
end
