class ChangeAdminPrivsType < ActiveRecord::Migration
  def change
    change_column :admin_privs, :conferences, :string
    change_column :admin_privs, :cfps, :string
  end
end
