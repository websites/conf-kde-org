class AddAddtionalFieldsToSponsorships < ActiveRecord::Migration
  def change
  	add_column :sponsorships, :accomodation_costs, :float
  	add_column :sponsorships, :tavel_costs, :float
  	add_column :sponsorships, :book_accomodation, :integer
  	add_column :sponsorships, :talk_proposed, :integer
  	add_column :sponsorships, :general_info, :text
  end
end
