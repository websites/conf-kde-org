class EnableConferencePluginOptions < ActiveRecord::Migration
  def up
  	#conferences
  	add_column :conferences, :enable_registration, :int
  	add_column :conferences, :enable_sponsorship, :int
  end

  def down
  end
end
