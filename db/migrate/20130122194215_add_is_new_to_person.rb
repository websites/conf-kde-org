class AddIsNewToPerson < ActiveRecord::Migration
  def change
    add_column :people, :is_new, :int
  end
end