class AddMoreInfoToRegistration < ActiveRecord::Migration
  def change
  	add_column :registrations, :role_in_kde, :string
  	add_column :registrations, :other_role, :text
  	add_column :registrations, :first_time, :integer
  	add_column :registrations, :dietery_simple, :string
  	add_column :registrations, :other_dietery, :text
  	add_column :registrations, :allergy, :text
  	add_column :registrations, :interests, :text
  	add_column :registrations, :emergency_contact, :text
  	add_column :registrations, :other_info, :text
  end
end
