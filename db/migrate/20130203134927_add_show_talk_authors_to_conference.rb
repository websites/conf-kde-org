class AddShowTalkAuthorsToConference < ActiveRecord::Migration
  def change
    add_column :conferences, :hide_talk_authors, :integer
  end
end
