class CreateAdminPrivs < ActiveRecord::Migration
  def change
    create_table :admin_privs do |t|
      t.integer "user_id"
      t.date    "conferences"
      t.date    "cfps"
      t.timestamps "datetime"
    end
  end
end
