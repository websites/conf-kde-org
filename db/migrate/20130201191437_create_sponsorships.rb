class CreateSponsorships < ActiveRecord::Migration
  def change
    create_table :sponsorships do |t|
      t.integer "user_id"
      t.integer "conference_id"
      t.string "user_contribution"
      t.timestamps
    end
  end
end
