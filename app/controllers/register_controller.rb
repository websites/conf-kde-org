class RegisterController < ApplicationController
  def tshirt
    @bookit = BookTshirt.where("user_id = ? and conference_id = ?", @current_user.id, @conference.id).first
    if not @bookit
      @bookit = BookTshirt.new
    end
    if params[:commit]
      @bookit.update_attributes(params[:book_tshirt])
      @bookit.conference_id = @conference.id
      @bookit.user_id = @current_user.id
      @bookit.save!
      flash[:alert] = "Thank you - your preference has been updated."
    end
  end

  def index
    @registration = Registration.where("user_id = ? and conference_id = ?", @current_user.id, @conference.id).first

    if params[:commit] == "Save Registration Information"
      @registration.will_attend = params[:registration]["will_attend"]
      if @registration.will_attend == 1
        flash[:alert] = "Thank you for registering to attend this conference"
      else
        flash[:alert] = "We regret to see that you will not be attending"
      end
      @registration.arrival_date = params[:registration]["arrival_date"]
      @registration.departure_date = params[:registration]["departure_date"]

      #@registration.role_in_kde = params[:registration]["role_in_kde"]
      @registration.update_attributes(params[:registration])

      @registration.other_role = params[:registration]["other_role"]
      @registration.first_time = params[:registration]["first_time"]
      @registration.dietery_simple = params[:registration]["dietery_simple"]
      @registration.other_dietery = params[:registration]["other_dietery"]
      @registration.allergy = params[:registration]["allergy"]
      @registration.interests = params[:registration]["interests"]
      @registration.emergency_contact = params[:registration]["emergency_contact"]
      @registration.other_info = params[:registration]["other_info"]
      @registration.enable_sponsor = params[:registration]["enable_sponsor"]

      @registration.save!
      
      #log this registration
      logger = RegistrationLogging.new
      logger.user_id = @current_user.id
      logger.will_attend = @registration.will_attend
      logger.current_count = Registration.where("conference_id = ? and will_attend = 1", @conference.id).count
      logger.save!
    end

    if not @registration
      @registration = Registration.new
      @registration.conference_id = @conference.id
      @registration.user_id = @current_user.id
      @registration.will_attend = 1 
      @registration.save
    end
  end
end
