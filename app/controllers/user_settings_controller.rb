class UserSettingsController < ApplicationController
  def index
    if params[:commit] == "Save Settings"
      @user_settings = UserSettings.where("user_id = ?", @current_user.id).first
      @user_settings.may_need_sponsorship = params[:user_settings]['may_need_sponsorship']
      @user_settings.save
      
      redirect_to my_settings_path, :notice => "Settings have been saved"
    end
    
    if not @user_settings = UserSettings.where("user_id = ?", @current_user.id).first
      @user_settings = UserSettings.new
      @user_settings.user_id = @current_user.id
      @user_settings.save
    end
  end
end
