class ConfsController < ApplicationController
  def index
    @confs = Conference.find(:all)
    @cfps = CallForPapers
  end
end