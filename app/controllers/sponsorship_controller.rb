class SponsorshipController < ApplicationController
  def index
  	if params[:commit] == "Apply for Sponsorship"
          @sponsorship = Sponsorship.where("user_id = ? and conference_id = ?", @current_user.id, @conference.id).first
          @sponsorship.update_attributes(params[:sponsorship])
          @sponsorship.accomodation_costs = params[:sponsorship]['accomodation_costs']
          @sponsorship.tavel_costs = params[:sponsorship]['tavel_costs']
          @sponsorship.book_accomodation = params[:sponsorship]['book_accomodation']
          @sponsorship.talk_proposed , = params[:sponsorship]['talk_proposed']
          @sponsorship.user_contribution = params[:sponsorship]['user_contribution']
          @sponsorship.general_info = params[:sponsorship]['general_info']
          @sponsorship.user_contribution = params[:sponsorship]['user_contribution']
          @sponsorship.share_room = params[:sponsorship]['share_room']
          @sponsorship.save

          if @sponsorship.tavel_costs.to_s.length == 0 or @sponsorship.user_contribution.to_s.length == 0 or @sponsorship.email.to_s.length == 0
            redirect_to sponsorship_request_path, :notice => "Some fields have not been filled out"
            return
          end
          redirect_to sponsorship_request_path, :notice => "Your request has been submitted"
  	end

    if not @sponsorship = Sponsorship.where("user_id = ? and conference_id = ?", @current_user.id, @conference.id).first
      @sponsorship = Sponsorship.new
      @sponsorship.user_id = @current_user.id
      @sponsorship.conference_id = @conference.id
      @sponsorship.email = @current_user.person.email
      @sponsorship.from_date = Registration.where("user_id = ? and conference_id = ?", @current_user.id, @conference.id).first.arrival_date
      @sponsorship.to_date = Registration.where("user_id = ? and conference_id = ?", @current_user.id, @conference.id).first.departure_date
      @sponsorship.save
    end

    if(@sponsorship.user_contribution and @sponsorship.user_contribution.length > 0)
      @notice = 'It appears you have already submitted a request for sponsorship, we will let you know in due time if you have been succesfull.'
    end
  end
end
