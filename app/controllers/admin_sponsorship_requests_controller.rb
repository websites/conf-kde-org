class AdminSponsorshipRequestsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :require_sponsorship_admin

  def index
  	@sponsors = Sponsorship.where("conference_id = ?", @conference.id)
  end

  def view
  	@sponsorship = Sponsorship.where("id = ?", params[:id]).first
  end
end
