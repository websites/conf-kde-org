require 'net/ldap'
class LdapTestController < ApplicationController
  
  def index
    @config = YAML.load(ERB.new(File.read("#{Rails.root}/config/ldap.yml")).result)[Rails.env]
      ldap = Net::LDAP.new(
        :host => @config['host'], # active_directory.atomicobject.com
        :port => @config['port'], # 389
        :base => @config['base'], # dc=atomicobject,dc=com
        :auth => {
          :method => :simple,
          :username => "#{@config['attribute']}=kcoyle,#{@config['base']}", # cn=server,dc=atomicobject,dc=com
          :password => "j9t4\GdFeuX" # server password
        }
      )
      filter = Net::LDAP::Filter.eq('uid', 'kcoyle')
      if ldap.bind_as(:filter => filter, :password => 'j9t4\GdFeuX')
        true
      else
        false
      end
  end
end