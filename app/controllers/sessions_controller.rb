class SessionsController < ApplicationController

  layout 'signup'

  before_filter :authenticate_user!, :only => :destroy
  before_filter :check_pentabarf_credentials, :only => :create

  def new
    @user = User.new
  end

  def create
    if params[:user][:password] == "to_be_ignored"
      @user = User.new
      flash[:alert] = t(:error_signing_in) 
      render :action => "new"
      return
    end
    @user = User.confirmed.find_by_email(params[:user][:email])
    if @user and @user.authenticate(params[:user][:password])
      login_as @user
      redirect_to successful_sign_in_path, :notice => t(:sign_in_successful)
      return
    else
      @config = YAML.load(ERB.new(File.read("#{Rails.root}/config/ldap.yml")).result)[Rails.env]
      ldap = Net::LDAP.new(
        :host => @config['host'], # active_directory.atomicobject.com
        :port => @config['port'], # 389
        :base => @config['base'], # dc=atomicobject,dc=com
        :auth => {
          :method => :simple,
          :username => "#{@config['attribute']}=#{params[:user]['email']},#{@config['base']}", # cn=server,dc=atomicobject,dc=com
          :password => "#{params[:user]['password']}"
        }
      )
      filter = Net::LDAP::Filter.eq(@config['attribute'], params[:user]['email'])
      if ldap.bind_as(:filter => filter, :password => params[:user]['password'])
        if (User.where('ldap_uid = ?', params[:user]['email']).count > 0) and @user = User.where('ldap_uid = ?', params[:user]['email']).first
          login_as @user
          redirect_to successful_sign_in_path, :notice => t(:sign_in_successful)
          return
        else
          filter = Net::LDAP::Filter.eq(@config['attribute'], params[:user]['email'])
          attrs = ["mail", "ircNick", "givenName", "sn", @config['atrribute']]
          ircNick = "."
          mail = "."
          fn = "."
          sn = "."
          ldap.search(:base => @config['base'], :attributes => attrs, :filter =>  filter,  :return_result => false) do |entry|
            if entry.respond_to?('mail')
              mail = entry.mail.to_s.gsub(/[\"\[\]]/,"")
            end
            if entry.respond_to?('ircNick') 
              ircNick = entry.ircNick.to_s.gsub(/[\"\[\]]/,"")
            end
            if entry.respond_to?('givenName')
              fn = entry.givenName.to_s.gsub(/[\"\[\]]/,"")
            end
            if entry.respond_to?('sn') 
              sn = entry.sn.to_s.gsub(/[\"\[\]]/,"")
            end
          end
          #create new user
          @user = User.new
          @user.email = mail
          @user.password = "to_be_ignored"
          @user.role = "submitter"
          @user.confirmed_at = Time.now
          @user.confirmation_token = nil
          @user.ldap_uid = params[:user][:email]
          @user.skip_confirmation!
          
          person = Person.new
          person.first_name = fn
          person.last_name = sn
          person.email = mail
          person.public_name = ircNick
          person.country = "."
          person.save!
          
          @user.person = person
          @user.save
          
          login_as @user
          redirect_to successful_sign_in_path, :notice => t(:sign_in_successful)
          return
        end
      else
        false
      end
      @user = User.new
      flash[:alert] = t(:error_signing_in) 
      render :action => "new"
      return
    end
  end

  def destroy
    reset_session
    redirect_to scoped_sign_in_path 
  end

  protected

  def successful_sign_in_path
    if current_user.role == "submitter"
      person = Person.where('user_id = ?', current_user.id).first
      if person
        current_user.person = person
        current_user.save
        if current_user.person.is_new != 1
          new_account_path
        end
      end
      confs_path
      if current_user.person.is_new != 1
        new_account_path
      else
        confs_path
      end
    else
      root_path
    end
  end

  def check_pentabarf_credentials
    User.check_pentabarf_credentials(params[:user][:email], params[:user][:password])
  end

end
