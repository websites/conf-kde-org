class ManageAdministratorsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :require_admin
  
  def index
    @admins = AdminPrivs.find(:all)
    @users = User
    @conferences = Conference.new
    @cfps = CallForPapers.new
  end
  
  def user
    if params[:commit]
      user_id = params[:admin_privs]['user_id']
      cfps = params[:admin_privs]['cfps']
      conferences = params[:admin_privs]['conferences']
      sponsorships = params[:admin_privs]['sponsorships']
      view_people_privs = params[:admin_privs]['view_people_privs']

      if AdminPrivs.where('user_id = ?', user_id).count > 0
        adminprivs = AdminPrivs.where('user_id = ?', user_id).first
        adminprivs.cfps = cfps
        adminprivs.conferences = conferences
        adminprivs.sponsorships = sponsorships
        adminprivs.view_people_privs = view_people_privs
        adminprivs.save
      else
        adminprivs = AdminPrivs.new
        adminprivs.user_id = user_id
        adminprivs.cfps = cfps
        adminprivs.conferences = conferences
        adminprivs.sponsorships = sponsorships
        adminprivs.view_people_privs = view_people_privs
        adminprivs.save
      end
    end
    
    if not params[:u]
      @user = User
      @adminprivs = AdminPrivs
    else
      if not @user = User.where("id = ?", params[:u]).first
        @user = User
        @adminprivs = AdminPrivs
      end
      @adminprivs = AdminPrivs.where("user_id = ?", params[:u]).first
    end
  end
end
