class NewAccountController < ApplicationController
  def index
    if params[:commit] == "Update Profile"
      person = @current_user.person
      person.first_name = params[:person]["first_name"]
      person.last_name = params[:person]["last_name"]
      person.public_name = params[:person]["public_name"]
      person.gender = params[:person]['gender']
      person.email = params[:person]["email"]
      person.email_public = params[:person]["email_public"]
      person.country = params[:person]["country"]
      person.is_new = 1

      person.update_attributes(params[:person])
      
      if person.email.length < 2
        redirect_to new_account_path, :notice => "Sorry, Email is a required field."
        return
      end
      if person.country.length < 1
        redirect_to new_account_path, :notice => "Sorry, Country is a required field."
        return
      end
      
      person.save!
      
      redirect_to confs_path, :notice => "Thank you for registering on conf.kde.org"
    end
  end
end
