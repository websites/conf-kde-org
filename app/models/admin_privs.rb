class AdminPrivs < ActiveRecord::Base
  # attr_accessible :title, :body
  def can_admin_sponsorships user, sponsorship_id
    if user.role == "admin"
      return true
    end
    if AdminPrivs.where("user_id = ?", user.id).count > 0
      sponsorships = AdminPrivs.where("user_id = ?", user.id).first.sponsorships.split(',')
      if sponsorships.count > 0
        sponsorships.each do |sponsorship|
          if sponsorship_id.to_i == sponsorship.to_i
            return true
          end
        end
      end
    end
    false
  end

  def can_admin_conference user, conference_id
    if user.role == "admin"
      return true
    end
    if AdminPrivs.where("user_id = ?", user.id).count > 0
      conferences = AdminPrivs.where("user_id = ?", user.id).first.conferences.split(',')
      conferences.each do |confid|
        if conference_id.to_i == confid.to_i
          return true
        end
      end
    end
    false
  end

  def can_admin_people_view user, conference_id
    if user.role == "admin"
      return true
    end
    if AdminPrivs.where("user_id = ?", user.id).count > 0
      conferences = AdminPrivs.where("user_id = ?", user.id).first.view_people_privs.split(',')
      conferences.each do |confid|
        if conference_id.to_i == confid.to_i
          return true
        end
      end
    end
    false
  end
  
  def can_admin_cfp user, cfp_id
    if user.role == "admin"
      return true
    end
    if AdminPrivs.where("user_id = ?", user.id).count > 0
      cfps = AdminPrivs.where("user_id = ?", user.id).first.cfps.split(',')
      cfps.each do |cfpid|
        if cfpid.to_i == cfp_id.to_i
          return true
        end
      end
    end
    false
  end

  def set_admin_conference user, conference_id
    if not can_admin_conference user conference_id
      if AdminPrivs.where("user_id = ?", user.id).count == 0
        admin = AdminPrivs.new
        admin.user_id = user.id
        admin.conferences = conference_id
        admin.save
      else
        admin = AdminPrivs.where("user_id = ?", user.id).first
        admin.conferences = "#{admin.conferences}, #{conference_id}"
        admin.save
      end
    end
  end
  
  def set_admin_cfp user, cfp_id
    if not can_admin_cfp user cfp_id
      if AdminPrivs.where("user_id = ?", user.id).count == 0
        admin = AdminPrivs.new
        admin.user_id = user.id
        admin.cfps = cfp_id
        admin.save
      else
        admin = AdminPrivs.where("user_id = ?", user.id).first
        admin.conferences = "#{admin.cfps}, #{cfp_id}"
        admin.save
      end
    end
  end
end
